import { ASubject } from './a.subject';
import { stamp, slug, fromStamp } from './functions';
import { db } from './db.service';
import { settings } from './settings.service';
import { apiService } from './api.service';

class Tasks {

    constructor() {
        this.offsetSeconds = 0;
        this.currentTask = new ASubject();
        this.days = new ASubject();
        settings.settings.subscribe((data) => {
            if (data['TIME_OFFSET']) {
                let offsetParts = data['TIME_OFFSET']['value'].split(':');
                this.offsetSeconds = offsetParts[0] * 3600 + offsetParts[1] * 60;
            }
            this.reloadCurrentTask();
        });

        let sql = 'CREATE TABLE IF NOT EXISTS items(' +
            'name TEXT' +
            ', slug TEXT' +
            ', startAt INTEGER' +
            ', stopAt  INTEGER' +
            ', seconds INTEGER' +
            ', logged  INTEGER' +
            ', ticket  TEXT' +
            ')';

        db.run(sql, []).then( async () => {

            let row = await db.get("SELECT * FROM items WHERE stopAt = 0 ORDER BY startAt DESC", []);

            if (row) {
                this.currentTask.next(row.name);
            } else {
                this.currentTask.next('');
            }
        });
    }

    async ticketParts(name) {
        let ticket = apiService.ticketByName(name);
        let data = await apiService.getTicketSummary(ticket);
        let summary = name;
        if (data['summary']) {
            summary = ticket + ': ' + data['summary'];
            if (name.split('---').length > 1) {
                name = summary + '---' + name.split('---')[1];
            } else if (name.replace(/\W+/g, '') != summary.replace(/\W+/g, '')) {
                let ticketSpells = apiService.getTicketSpellVersions(ticket);
                for (let spell of ticketSpells) {
                    name = name.replace(spell, '');
                }
                name = name.replace(/^\W+|\s+$/, '');
                name = summary + '---' + name;
            } else {
                name = summary;
            }
        }

        return {
            ticket: ticket,
            name: name
        }
    }

    async insertNewTask(name, slug, startAt, stopAt, seconds, logged, ticket) {
        if (startAt instanceof Date) {
            startAt = stamp(startAt);
        }
        if (stopAt instanceof Date) {
            stopAt = stamp(stopAt);
        }

        return await db.run(
            'INSERT INTO items VALUES(?, ?, ?, ?, ?, ?, ?)',
            [name, slug, startAt, stopAt, seconds, logged, ticket]
        );
    }

    async addNewTask(name, startAt, stopAt) {
        let seconds = stopAt ? stopAt - startAt : 0;
        let parts = await this.ticketParts(name);
        await this.insertNewTask(parts['name'], slug(parts['name']), startAt, stopAt, seconds, 0, parts['ticket']);
        this.reloadCurrentTask();
    }

    async reloadCurrentTask() {
        let row = await db.get("SELECT * FROM items WHERE stopAt = 0 ORDER BY startAt DESC", []);

        if (row) {
            this.currentTask.next(row.name);
        } else {
            this.currentTask.next('');
        }
    }

    async startTask(name) {
        let time = stamp(new Date());
        await this.stopTask(false);
        let parts = await this.ticketParts(name);
        await this.insertNewTask(parts['name'], slug(parts['name']), time, 0, 0, 0, parts['ticket']);
        this.currentTask.next(parts['name']);
    }

    async stopTask(emptyCurrentTask = true) {
        if (this.currentTask.data) {
            let time = stamp(new Date());

            let row = await db.get("SELECT rowid, * FROM items WHERE stopAt = 0 ORDER BY startAt DESC", []);
            if (row && row.rowid) {
                let sec = time - row.startAt;
                await db.run('UPDATE items SET stopAt = ?, seconds = ? WHERE rowid = ?', [time, sec, row.rowid]);
            }
            if (emptyCurrentTask) {
                this.currentTask.next('');
            }
        }
    }

    async loadTasksBetween(startDate, endDate) {
        let rows = await db.all(
            "SELECT rowid, * FROM items WHERE startAt >= ? AND startAt < ? ORDER BY startAt DESC",
            [stamp(startDate), stamp(endDate)]
        );

        return rows;
    }

    async loadDaysBetween(startDate, endDate) {
        let days = [];
        let date = new Date();
        date.setTime(startDate.getTime());
        date.setHours(0, 0, 0 ,0);

        while (date.getTime() < endDate.getTime()) {

            let dateObj = new Date();
            dateObj.setTime(date.getTime());
            let day = {
                name: date.toLocaleDateString(),
                dateObj: dateObj,
                tasks: []
            };

            let dateNext = new Date();
            dateNext.setTime(date.getTime());
            dateNext.setDate(dateNext.getDate() + 1);

            //select tasks
            let rows = await db.all(
                "SELECT rowid, * FROM items WHERE startAt >= ? AND startAt < ? ORDER BY startAt DESC",
                [stamp(date) + this.offsetSeconds, stamp(dateNext) + this.offsetSeconds]
            );

            rows.forEach((row) => {
                day.tasks.push(row);
            });
            this.days.next(days);
            days.push(day);
            date.setDate(date.getDate() + 1);
        }
        return days;
    }

    async loadDays() {

        let days = [];

        let date = new Date();
        date.setHours(0, 0, 0 ,0);
        for (let i of [0,1,2,3,4,5,6]) {

            let day = {
                name: date.toLocaleDateString(),
                tasks: []
            };

            let dateNext = new Date();
            dateNext.setTime(date.getTime());
            dateNext.setDate(dateNext.getDate() + 1);

            //select tasks
            let rows = await db.all(
                "SELECT rowid, * FROM items WHERE startAt >= ? AND startAt < ? ORDER BY startAt DESC",
                [stamp(date) + this.offsetSeconds, stamp(dateNext) + this.offsetSeconds]
            );

            rows.forEach((row) => {
                day.tasks.push(row);
            });

            days.push(day);
            date.setDate(date.getDate() - 1);
        }

        this.days.next(days);
        return days;
    }

    async deleteTask(rowId) {
        await db.run('DELETE FROM items WHERE rowid = ?', [rowId]);
        this.loadDays();
        this.currentTask.next(this.currentTask.data);
    }

    continueTask(name) {
        this.startTask(name);
    }

    async replaceLastMinutes(name, seconds) {
        let now = stamp(new Date());
        let timeFrom = now - seconds;

        await db.run('DELETE FROM items WHERE startAt > ?', [timeFrom]);

        let rows = await db.all('SELECT rowid, * FROM items WHERE stopAt > ? OR stopAt = 0', [timeFrom]);
        if (rows && rows.length && rows.length > 0) {
            for (let row of rows) {
                await db.run('UPDATE items SET stopAt = ?, seconds = ? - startAt WHERE rowid = ?', [timeFrom - 1, timeFrom - 1, row.rowid]);
            }
        }

        let parts = await this.ticketParts(name);
        await this.insertNewTask(parts['name'], slug(parts['name']), timeFrom, now, seconds, 0, parts['ticket']);
        await this.stopTask();
        this.loadDays();
    }

    async logTask(rowId, comment, remaining, ticket) {
        let row = await db.get('SELECT * FROM items WHERE rowid = ?', [rowId]);
        if (row) {
            ticket = ticket || row.ticket;
            if (!ticket) {
                console.error(`Has no ticket on row ${row.name} ${row.rowid}`, row);
                return;
            }
            apiService.postWorkLog(ticket, row.startAt - this.offsetSeconds, row.seconds, comment, remaining);
            await db.run('UPDATE items SET logged = ? WHERE rowid = ?', [1, rowId]);
            this.reloadCurrentTask();
            return name;
        }
        throw `Not found rowId ${rowId}`;
    }

    getTaskTicket(task) {
        return task.name.split('---')[0];
    }

    getTaskActivity(task) {
        let parts = task.name.split('---');
        return parts.length > 1 ? parts[1] : '-';
    }

    async rename(rowId, name) {
        let row = await db.get('SELECT * FROM items WHERE rowid = ?', [rowId]);
        if (row) {
            let parts = await this.ticketParts(name);
            await db.run('UPDATE items SET name = ?, slug = ?, ticket = ? WHERE rowid = ?', [parts['name'], slug(parts['name']), parts['ticket'], rowId]);
            this.reloadCurrentTask();
            return parts['name'];
        }
        return false;
    }

    async findSimilarNotLoggedTasks(name, time) {
        let short = slug(name);

        let stampWithOffset = time - this.offsetSeconds;
        let date = fromStamp(stampWithOffset);
        date.setHours(0, 0, 0, 0);

        let fromTime = stamp(date) + this.offsetSeconds;
        let toTime = fromTime + 3600 * 24;

        let rows = await db.all('SELECT rowid, * FROM items WHERE slug = ? AND logged = 0 AND startAt >= ? AND startAt < ? ORDER BY startAt DESC', [short, fromTime, toTime]);
        if (rows && rows.length) {
            return rows;
        }
        return [];
    }

    async updateLoggedTask(rowId, seconds, name) {
        await db.run('UPDATE items SET seconds = ?, logged = 1, name = ?, slug = ? WHERE rowid = ?', [seconds, name, slug(name), rowId]);
        this.reloadCurrentTask();
    }

    async updateStartAt(rowId, time) {
        if (time === 0) {
            console.log('ERROR: start at time can not be 0');
        }
        let row = await db.get('SELECT * FROM items WHERE rowid = ?', [rowId]);
        if (row) {
            let seconds = row.stopAt - time;
            await db.run('UPDATE items SET startAt = ?, seconds = ? WHERE rowid = ?', [time, seconds, rowId]);
            this.reloadCurrentTask();
            return name;
        }
        return false;
    }

    async updateStopAt(rowId, time) {
        let row = await db.get('SELECT * FROM items WHERE rowid = ?', [rowId]);
        if (row) {
            let seconds = time - row.startAt;
            if (time === 0) {
                seconds = 0;
            }
            await db.run('UPDATE items SET stopAt = ?, seconds = ? WHERE rowid = ?', [time, seconds, rowId]);
            this.reloadCurrentTask();
            return name;
        }
        return false;
    }
}


export let tasks = new Tasks();

