const http = require('http');
const https = require('https');
const querystring = require('querystring');
import { settings } from './settings.service';

export class ClickUpAPI {

    constructor() {
        this.apiUrl = settings.getValue('API_URL');
        this.authToken = settings.getValue('CLICK_UP_API_TOKEN');
        this.teamId = settings.getValue('CLICK_UP_TEAM_ID');
        this.userId = settings.getValue('CLICK_UP_USER_ID');
    }

    static support(url) {
        return url.match(/\.clickup\.com/);
    }

    generateLink(ticket) {
        if (!ticket) {
            return '';
        }
        return 'https://app.clickup.com/t/' + this.normalizeTicketNumber(ticket);
    }

    async getTicketSummary(ticket) {
        ticket = this.normalizeTicketNumber(ticket);
        let content = await this.doRequest(this.apiUrl + '/task/' + ticket);
        // console.log('Got from server', content);
        let data = JSON.parse(content);
        if (data) {
            return {
                summary: data['name'],
                status: data['status']['status'],
                timetracking: data['time_spent']
            }
        }
        return null;
    }

    async postWorkLog(ticket, startAt, seconds, description, remaining) {
        ticket = this.normalizeTicketNumber(ticket);
        let url = this.apiUrl + 'task/' + ticket + '/time';

        let startDate = new Date();
        startDate.setTime(startAt * 1000);

        const time = seconds * 1000;

        let data = {
            start: startDate.getTime(),
            end: startDate.getTime() + time,
            time: time
        };

        return await this.doRequest(url, data);
    }

    async loadWorkLogsBetweenDates(startAt, stopAt) {
        return [];
    }

    async loadToWorkTickets() {
        const url = `${this.apiUrl}/team/${this.teamId}/task?archived=false&assignees[]=${this.userId}`;
        let content = await this.doRequest(url);
        let data = JSON.parse(content);

        let toWork = [];

        for (let task of data['tasks']) {
            let item = {};
            item['issue'] = task.id;
            item['summary'] = task.name;
            item['link'] = this.generateLink(task.id);
            // item['remaining'] = Math.round(10.0 * eta / 3600) / 10;
            // item['percent'] = Math.floor(Math.round(100 - 100.0 * eta / (eta + spent)));
            item['remaining'] = 0;
            item['percent'] = 0;

            toWork.push(item);
        }

        return toWork;
    }

    ticketByName(name) {
        const pattern = new RegExp('(^|\\W)(' + 'CU\\-\\w+|#\\w+' + ')');
        let m = name.match(pattern);

        if (m) {
            return m[2];
        }

        // Get by link
        m = name.match(/\/t\/(\w+)/g);

        if (m) {
            return 'CU-' + m[0].replace('/t/', '');
        }

        return '';
    }

    getTicketSpellVersions(ticket) {
        let ticketNum = ticket.replace(/(CU\-|#)(\w+)/, "$2");
        
        return ['#' + ticketNum, 'CU-' + ticketNum, 'https://app.clickup.com/t/' + ticketNum];
    }

    doRequest(url, postData) {
        let options = this.makeBasicOptions(url);
        let body = null;
        if (postData) {
            body = JSON.stringify(postData);
            options.method = 'POST';

            options.headers['Content-Type'] = 'application/json';
            options.headers['Content-Length'] = Buffer.byteLength(body);
        }

        options.headers['Authorization'] = this.authToken;

        let scheme = http;
        if (this.apiUrl.match(/^https/)) {
            scheme = https;
        }

        return new Promise((resolve, reject) => {
            let content = '';
            const req = scheme.request(options, (res) => {
                // console.log(`STATUS: ${res.statusCode}`);
                // console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
                res.setEncoding('utf8');
                res.on('data', (chunk) => {
                    content += chunk;
                });
                res.on('end', () => {
                    // console.log(`CONTENT: ${content}`);
                    resolve(content);
                });
            });

            req.on('error', (e) => {
                console.error(`problem with request: ${e.message}`);
                reject(e);
            });

            if (postData) {
                req.write(body);
            }

            req.end();
        });
    }

    makeBasicOptions(url) {
        let m = url.match(/^(\w+)\:\/\/([^\/]+)(:(\d+))?(\/.*)$/);
        if (!m) {
            throw 'apiURL is incorrect';
        }
        let port = 80;
        if (m[1] == 'https') {
            port = 443;
        }
        return {
            hostname: m[2],
            port: (m[3] ? m[4] : port),
            path: m[5],
            agent: false,
            headers: {},
        };
    }

    normalizeTicketNumber(ticket) {
        return ticket.replace(/CU-|#/g, '');
    }
}