import { JiraAPI } from './jira.api';
import { ClickUpAPI } from './click-up.api';
import { settings } from './settings.service';

class APIService {

    constructor() {
        this.api = null;
        settings.settings.subscribe((data) => {
            if (data.API_URL && data.API_URL.value) {
                this.setIntegrationURL(data.API_URL.value);
            }
        });
    }

    generateLink(ticket) {
        if (!this.api) {
            return '';
        }
        return this.api.generateLink(ticket);
    }

    async getTicketSummary(ticket) {
        if (!this.api || !ticket) {
            return '';
        }
        return await this.api.getTicketSummary(ticket);
    }

    getTicketSpellVersions(ticket) {
        if (!this.api) {
            return [ticket];
        }
        return this.api.getTicketSpellVersions(ticket);
    }

    async postWorkLog(ticket, startAt, seconds, description, remaining) {
        if (!this.api) {
            return '';
        }
        return await this.api.postWorkLog(ticket, startAt, seconds, description, remaining);
    }

    async loadWorkLogsBetweenDates(startAt, stopAt) {
        if (!this.api) {
            return [];
        }
        return await this.api.loadWorkLogsBetweenDates(startAt, stopAt);
    }

    async loadToWorkTickets() {
        if (!this.api) {
            return [];
        }
        return await this.api.loadToWorkTickets();
    }

    setIntegrationURL(url) {
        if (JiraAPI.support(url)) {
            this.api = new JiraAPI();
        } else if (ClickUpAPI.support(url)) {
            this.api = new ClickUpAPI();
        }
    }

    ticketByName(name) {
        if (!this.api) {
            return [];
        }

        return this.api.ticketByName(name);
    }
}

export let apiService = new APIService();

window.apiService = apiService;