import React from 'react';
import { stamp, datetime } from '../functions';
import { ModalData } from './modal-data';
import { modalsControl } from './modals-control';

export class AddCustomTaskModal extends React.Component {

    name() {
        return 'AddCustomTaskModal';
    }
    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    createState() {
        return {
            modal: new ModalData(false, null, '', '', ''),
            name: '',
            startAt: datetime(new Date()),
            stopAt: datetime(new Date())
        }
    }

    constructor(props) {
        super(props);
        this.state = this.createState();

        modalsControl.modalsSubject.subscribe((data) => {

            if (data[this.name()]) {
                let state = this.createState();
                state.modal = data[this.name()];

                this.setState(state);
            }
        });
    }

    updateName(e) {
        let state = Object.assign({}, this.state);
        state.name = e.target.value;
        this.setState(state);
    }

    updateStartAt(e) {
        let state = Object.assign({}, this.state);
        state.startAt = e.target.value;
        this.setState(state);
    }

    updateStopAt(e) {
        let state = Object.assign({}, this.state);
        state.stopAt = e.target.value;
        this.setState(state);
    }

    onButtonClick(e) {
        let callback = this.state.modal.callback;
        if (callback) {

            let startAt = new Date(this.state.startAt);
            let stopAt = new Date(this.state.stopAt);
            if (isNaN(startAt.getTime())) {
                return;
            }

            callback(
                this.state.name,
                stamp(startAt),
                !isNaN(stopAt.getTime()) ? stamp(stopAt) : 0
            );
        }
        modalsControl.hideModal(this.name());
    }

    render() {

        return (
            <div className={"modal add-custom-modal " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>Creating new custom task:</h3>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        Name:
                        <input type="text" value={this.state.name}
                               onChange={(e) => this.updateName(e)}
                               onKeyUp={(e) => this.updateName(e)} />
                    </div>
                    <div>
                        StartAt:
                        <input type="text" value={this.state.startAt}
                               onChange={(e) => this.updateStartAt(e)}
                               onKeyUp={(e) => this.updateStartAt(e)} />
                    </div>
                    <div>
                        StopAt:
                        <input type="text" value={this.state.stopAt}
                               onChange={(e) => this.updateStopAt(e)}
                               onKeyUp={(e) => this.updateStopAt(e)} />
                    </div>
                    <div>
                        <button onClick={(e)=>this.onButtonClick(e)}>Insert</button>
                    </div>
                </div>
            </div>
        );
    }
}