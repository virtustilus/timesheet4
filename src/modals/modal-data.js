export class ModalData {
    constructor(visible, callback, name, value, additional) {
        this.visible = visible;
        this.callback = callback;
        this.name = name;
        this.value = value;
        this.additional = additional;
    }
}

