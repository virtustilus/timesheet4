import React from 'react';
import { toTime, stamp, spentFormat, datetimeDate, dateTimeDateAndWeek, slug } from '../functions';
import { settings } from '../settings.service';
import { apiService } from '../api.service';
import { tasks } from '../tasks.service';
import { ModalData } from './modal-data';
import { modalsControl } from './modals-control';

export class ReportModal extends React.Component {

    name() {
        return 'ReportModal';
    }
    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    constructor(props) {
        super(props);
        this.state = {
            modal: new ModalData(false, null, null),
            date: '',
            report: ''
        };
        this.settings = {};
        settings.settings.subscribe((data) => {
            this.settings = data;
        });

        modalsControl.modalsSubject.subscribe((data) => {

            if (data[this.name()]) {
                let state = Object.assign({}, this.state);
                state.modal = data[this.name()];
                let d = new Date();
                if (d.getDay() == 1) {
                    d.setDate(d.getDate() - 3);
                } else if (d.getDay() == 0) {
                    d.setDate(d.getDate() - 2);
                } else {
                    d.setDate(d.getDate() - 1);
                }
                state.date = datetimeDate(d);
                this.setState(state);
            }
        });
    }

    // updateValue(e, setting) {
    //     settings.setValue(setting.name, e.target.value);
    // }

    changeDate(e) {
        let state = Object.assign({}, this.state);
        state.date = e.target.value;
        this.setState(state);
    }

    onButtonClick(e) {
        //collect all tickets and worklogs at first
        
        let dateEnd = new Date();
        const offsetMinutes = -dateEnd.getTimezoneOffset();
        const companyOffset = +(settings.getValue('COMPANY_TIME_OFFSET'));
        const CETOffset = companyOffset - offsetMinutes / 60;
        dateEnd.setHours(0, 0, 0, 0);
        dateEnd.setTime(dateEnd.getTime() + tasks.offsetSeconds * 1000);

        let dateStart = new Date(this.state.date);
        

        let state = Object.assign({}, this.state);
        state.report = "Loading, wait about 1 minute...";
        this.setState(state);

        let adHocPrefix = this.settings['ADHOC_TICKET'].value.split('-')[0];
        //console.log('adHocPrefix', adHocPrefix);

        setTimeout(async () => {
            //let workLogs = await apiService.loadWorkLogsBetweenDates(stamp(dateStart), stamp(dateEnd));
            let tickets = await apiService.loadToWorkTickets();

            // let workLogsByIssue = new Map();
            // for (let item of workLogs) {
            //     if (item['issue'].replace(/^(\w+)-\d+/, '$1') === adHocPrefix) {
            //         workLogsByIssue.set(item['issue'] + '-' + item['comment'], item);
            //     } else {
            //         if (workLogsByIssue.has(item['issue'])) {
            //             let obj = workLogsByIssue.get(item['issue']);
            //             obj['comment'] = obj['comment'] + ', ' + item['comment'];
            //             obj['worked'] += +item['worked'];
            //         } else {
            //             workLogsByIssue.set(item['issue'], item);
            //         }
            //     }
            // }

            // cycle by time
            dateStart.setHours(0, 0, 0, 0);
            dateStart.setTime(dateStart.getTime() + tasks.offsetSeconds * 1000);
            let listOfTasks = await tasks.loadTasksBetween(dateStart, dateEnd);
            listOfTasks = listOfTasks.reverse();
            listOfTasks = listOfTasks.filter(function(task){
                return task.stopAt !== null 
                        && task.stopAt !== 0 
                        && task.stopAt > task.startAt 
                        && task.stopAt - task.startAt >= 60
                        && task.name && !task.name.match(/\s*(Lunch|Private Time)\s*/i)
                        ;
            });

            let taskBySlugWorkedTotal = {};
            let tasksUnique = [];
            let ticketObjects = {}; //loaded from server

            for (let task of listOfTasks) {
                
                if (task.ticket) {
                    if (ticketObjects[task.ticket] === undefined) {
                        ticketObjects[task.ticket] = await apiService.getTicketSummary(task.ticket);
                    }
                    // task.slug = slug(ticketObjects[task.ticket]['summary']);
                    // task.name = ticketObjects[task.ticket]['summary'];
                }

                if (!taskBySlugWorkedTotal[task.slug]) {
                    taskBySlugWorkedTotal[task.slug] = 0;
                    tasksUnique.push(task);
                }
                task.seconds = task.stopAt - task.startAt;
                taskBySlugWorkedTotal[task.slug] += task.seconds;
            }

            let timeTotal = 0;
            for (let task of listOfTasks) {
                timeTotal += task.seconds;
            }


            // collapse identical tasks that goes after each other
            let i = 1;
            while (i < listOfTasks.length) {
                let j = i - 1;
                let task = listOfTasks[i];
                let prevTask = listOfTasks[j];
                if (task.slug == prevTask.slug &&  Math.abs(task.startAt-prevTask.stopAt) < 300) {
                    prevTask.stopAt = task.stopAt;
                    prevTask.seconds = prevTask.stopAt - prevTask.startAt;
                    task.seconds = 0;
                    listOfTasks.splice(i, 1);
                    continue;
                }
                i++;
            }

            let timeTotalText = spentFormat(timeTotal);
            let report = "Hello! \n\n1. I worked " + timeTotalText + " on:";

            for (let task of tasksUnique) {
                let total = taskBySlugWorkedTotal[task.slug];
                if (total < 60) {
                    continue;
                }
                report += "\n  - ";
                
                let ticket = task.ticket;

                let line = '';
                if (ticket && ticket.replace(/^(\w+)-\d+/, '$1') !== adHocPrefix) {
                    line = task.name + ' - ' + apiService.generateLink(ticket) + ' - ' 
                        + ticketObjects[ticket].status 
                        + ' - ' + spentFormat(total);
                } else {
                    line = task.name + ' - ' + spentFormat(total);
                }

                report += line;
            }

            report += "\n\n2. Plan to work on:\n";

            
            report += "\n";
            report += "\n";
            report += "\n";

            report += "\n\n3. Blockers: \n- Nothing blocking";

            report += "\n\n\n SAMPLES: \n\n";

            for (let item of tickets) {

                let dateEnd = new Date();
                dateEnd.setHours(0, 0, 0, 0);
                let eta = item['remaining'];
                let daysPlus = Math.floor(Math.ceil(eta / 5) - 1);
                daysPlus = Math.max(0, daysPlus);
                dateEnd.setDate(dateEnd.getDate() + daysPlus);

                report += `\n   - ${item['summary']} - ${item['link']} - ETA: ${item['remaining']}h, TO WORK: 000 h`;
            }

            let state = Object.assign({}, this.state);
            state.report = report;
            this.setState(state);
        }, 500);
    }

    render() {

        return (
            <div className={"modal report-modal " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>Report for a meeting:</h3>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        I set the start date for you, but you can change it:
                    </div>
                    <div>
                        <input type="text"
                               placeholder="Date like: Apr 12 2018"
                               value={this.state.date}
                               onChange={(e) => this.changeDate(e)}
                        />
                    </div>
                    <div>
                        <button onClick={(e) => this.onButtonClick(e)} > Generate report </button>
                    </div>
                    <textarea className="report-view" readOnly value={this.state.report} />
                </div>
            </div>
        );
    }
}
