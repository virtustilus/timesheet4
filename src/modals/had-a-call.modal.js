import React from 'react';
import { spentToSeconds, validateSpent } from '../functions';
import { ModalData } from './modal-data';
import { modalsControl } from './modals-control';

export class HadACallModal extends React.Component {

    name() {
        return 'HadACallModal';
    }

    constructor(props) {
        super(props);
        this.state = {
            modal: new ModalData(false, null, null),
            seconds: 0,
            desc: '',
            timeString: ''
        };

        modalsControl.modalsSubject.subscribe((data) => {

            if (data[this.name()]) {
                let v = data[this.name()];
                this.setState({
                    modal: v,
                    seconds: 0,
                    desc: '',
                    timeString: ''
                });
            }
        });
    }

    onTextChange(e) {
        this.setState({
            modal: this.state.modal,
            seconds: this.state.seconds,
            desc: e.target.value,
            timeString: this.state.timeString
        });
    }

    onButtonClick(e) {
        if (!this.state.seconds) {
            return;
        }
        if (this.state.modal.callback) {
            this.state.modal.callback(this.state.desc, this.state.seconds);
        }
        modalsControl.hideModal(this.name());
    }

    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    getSeconds() {
        return this.state.timeString;
    }

    onSecondsChange(e) {
        if (validateSpent(e.target.value)) {
            this.setState({
                modal: this.state.modal,
                seconds: spentToSeconds(e.target.value),
                desc: this.state.desc,
                timeString: e.target.value
            });
        }
    }

    render() {
        return (
            <div className={"modal had-a-call " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>I had a call</h3>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        Description:
                        <input type="text" placeholder="Description" onChange={(e)=>this.onTextChange(e)}/>
                    </div>
                    <div>
                        How long you had a call?
                        <input type="text" placeholder="Time like 2h 12m 3s" onChange={(e)=>this.onSecondsChange(e)} value={this.getSeconds()}/>
                    </div>
                    <div>
                        <button onClick={(e)=>this.onButtonClick(e)}>Insert or replace</button>
                    </div>
                </div>

            </div>
        );
    }
}