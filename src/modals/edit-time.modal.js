import React from 'react';
import { stamp, datetime } from '../functions';
import { ModalData } from './modal-data';
import { modalsControl } from './modals-control';

export class EditTimeModal extends React.Component {

    name() {
        return 'EditTimeModal';
    }
    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    constructor(props) {
        super(props);
        this.state = {
            modal: new ModalData(false, null, '', '', ''),
            time: ''
        };

        modalsControl.modalsSubject.subscribe((data) => {

            if (data[this.name()]) {
                let state = Object.assign({}, this.state);
                state.modal = data[this.name()];
                state.time = this.getStringValue(state.modal.value);
                this.setState(state);
            }
        });
    }

    updateValue(e) {
        let state = Object.assign({}, this.state);
        state.time = e.target.value;
        this.setState(state);
    }

    onButtonClick(e) {
        let callback = this.state.modal.callback;

        if (this.state.time === '') {
            callback(0);
        } else {
            let yearAgo = new Date();
            yearAgo.setFullYear(yearAgo.getFullYear() - 1);

            let newDate = new Date(this.state.time);
            if (isNaN(newDate.getTime())) {
                return;
            }
            if (newDate.getTime() < yearAgo.getTime()) {
                return;
            }
            callback(stamp(newDate));
        }

        modalsControl.hideModal(this.name());
    }

    getStringValue(time) {
        let d = new Date();
        d.setTime(time * 1000);
        return datetime(d);
    }

    render() {

        return (
            <div className={"modal edit-time-modal " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>Edit {this.state.modal.additional} time</h3>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        <input type="text" value={this.state.time}
                               onChange={(e) => this.updateValue(e)}
                               onKeyUp={(e) => this.updateValue(e)} />
                    </div>
                    <div>
                        <button onClick={(e)=>this.onButtonClick(e)}>Update</button>
                    </div>
                </div>
            </div>
        );
    }
}
