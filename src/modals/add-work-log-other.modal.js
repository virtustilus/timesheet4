import React from 'react';
import { ModalData } from './modal-data';
import { modalsControl } from './modals-control';

export class AddWorkLogOtherModal extends React.Component {

    name() {
        return 'AddWorkLogOtherModal';
    }
    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    createState() {
        return {
            modal: new ModalData(false, null, '', '', ''),
        }
    }

    constructor(props) {
        super(props);
        this.state = this.createState();

        modalsControl.modalsSubject.subscribe((data) => {

            if (data[this.name()]) {
                let state = this.createState();
                state.modal = data[this.name()];

                this.setState(state);
            }
        });
    }

    onButtonClick(type) {
        let callback = this.state.modal.callback;
        if (callback) {
            callback(type);
        }
        modalsControl.hideModal(this.name());
    }

    render() {

        return (
            <div className={"modal add-work-log-other-modal " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>Add other worklog:</h3>
                    <p> <b>Task</b> : {this.state.modal.name} </p>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        <button onClick={(e)=>this.onButtonClick('skype')}>Skype</button>
                    </div>
                    <div>
                        <button onClick={(e)=>this.onButtonClick('adhoc')}>AdHoc</button>
                    </div>
                </div>
            </div>
        );
    }
}