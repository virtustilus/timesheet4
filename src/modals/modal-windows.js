import React from 'react';
import { HadACallModal } from './had-a-call.modal';
import { SettingsModal } from './settings.modal';
import { EditDescModal } from './edit-desc.modal';
import { EditTimeModal } from './edit-time.modal';
import { AddCustomTaskModal } from './add-custom-task.modal';
import { AddWorkLogModal } from './add-work-log.modal';
import { AddWorkLogOtherModal } from './add-work-log-other.modal';
import { ReportModal } from './report.modal';
import { MonthReportModal } from './month-report.modal';

export class ModalWindows extends React.Component {

    render() {
        return (
            <div className="modals i">
                <HadACallModal />
                <SettingsModal />
                <EditDescModal />
                <EditTimeModal />
                <AddCustomTaskModal />
                <AddWorkLogModal />
                <AddWorkLogOtherModal />
                <ReportModal />
                <MonthReportModal />
            </div>
        );
    }
}
