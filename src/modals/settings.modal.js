import React from 'react';
import { settings } from '../settings.service';
import { ModalData } from './modal-data';
import { modalsControl } from './modals-control';

export class SettingsModal extends React.Component {

    name() {
        return 'SettingsModal';
    }
    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    constructor(props) {
        super(props);
        this.state = {
            modal: new ModalData(false, null, null)
        };

        modalsControl.modalsSubject.subscribe((data) => {

            if (data[this.name()]) {
                let state = Object.assign({}, this.state);
                state.modal = data[this.name()];
                this.setState(state);
            }
        });
    }

    updateValue(e, setting) {
        settings.setValue(setting.name, e.target.value);
    }

    renderInput(setting) {
        if (setting.type == 'text') {
            return (<textarea defaultValue={setting.value}
                              onChange={(e) => this.updateValue(e, setting)}
                              onKeyUp={(e) => this.updateValue(e, setting)}></textarea>);
        }
        if (setting.type == 'password') {
            return (<input type="password" defaultValue={setting.value}
                           onChange={(e) => this.updateValue(e, setting)}
                           onKeyUp={(e) => this.updateValue(e, setting)} />);
        }
        return (<input type="text" defaultValue={setting.value}
                       onChange={(e) => this.updateValue(e, setting)}
                       onKeyUp={(e) => this.updateValue(e, setting)} />);
    }

    render() {

        let sList = settings.getNames().map((name, i) => {
            let setting = settings.getObject(name);

            return (
                <div key={i} className="setting">
                    <div className="name">{setting.name}</div>
                    <div className="value">
                        {this.renderInput(setting)}
                    </div>
                </div>
            );
        });

        return (
            <div className={"modal settings-modal " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>Settings</h3>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        {sList}
                    </div>
                </div>
            </div>
        );
    }
}