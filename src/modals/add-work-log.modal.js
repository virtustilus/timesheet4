import React from 'react';
import { ModalData } from './modal-data';
import { modalsControl } from './modals-control';

export class AddWorkLogModal extends React.Component {

    name() {
        return 'AddWorkLogModal';
    }
    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    createState() {
        return {
            modal: new ModalData(false, null, '', '', ''),
            comment: '',
            remaining: '',
        }
    }

    constructor(props) {
        super(props);
        this.state = this.createState();
        this.input = null;

        modalsControl.modalsSubject.subscribe((data) => {

            if (data[this.name()]) {
                let state = this.createState();
                state.modal = data[this.name()];
                state.comment = state.modal.value;
                
                this.setState(state);
            }
            if (this.input) {
                this.input.focus();
            }
        });
    }

    updateComment(e) {
        let state = Object.assign({}, this.state);
        state.comment = e.target.value;
        this.setState(state);
        if (e.type && e.type === 'keyup') {
            if (e.keyCode === 13) {
                this.onButtonClick(e);
            }
            if (e.keyCode === 27) {
                this.onClose(e);
            }
        }
    }

    updateRemaining(e) {
        let state = Object.assign({}, this.state);
        state.remaining = e.target.value;
        this.setState(state);
        if (e.type && e.type === 'keyup') {
            if (e.keyCode === 13) {
                this.onButtonClick(e);
            }
            if (e.keyCode === 27) {
                this.onClose(e);
            }
        }
    }

    onButtonClick(e) {
        let callback = this.state.modal.callback;
        if (callback) {

            callback(
                this.state.comment,
                this.state.remaining
            );
        }
        modalsControl.hideModal(this.name());
    }

    render() {

        return (
            <div className={"modal add-work-log-modal " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>Add worklog:</h3>
                    <p> <b>Task</b> : {this.state.modal.name} </p>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        Comment:
                        <input type="text" value={this.state.comment}
                               ref={(ref) => {this.input = ref;}}
                               onChange={(e) => this.updateComment(e)}
                               onKeyUp={(e) => this.updateComment(e)} />
                    </div>
                    <div>
                        Remaning (example: 1h 2m or leave empty for automatic) :
                        <input type="text" value={this.state.remaining}
                               onChange={(e) => this.updateRemaining(e)}
                               onKeyUp={(e) => this.updateRemaining(e)} />
                    </div>
                    <div>
                        <button onClick={(e)=>this.onButtonClick(e)}>Insert</button>
                    </div>
                </div>
            </div>
        );
    }
}
