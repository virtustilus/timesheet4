import React from 'react';
import { spentFormat } from '../functions';
import { settings } from '../settings.service';
import { tasks } from '../tasks.service';
import { ModalData } from './modal-data';
import { modalsControl } from './modals-control';

export class MonthReportModal extends React.Component {
    name() {
        return 'MonthReportModal';
    }
    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    constructor(props) {
        super(props);
        this.state = {
            modal: new ModalData(false, null, null),
            date: '',
            report: ''
        };
        this.settings = {};
        settings.settings.subscribe((data) => {
            this.settings = data;
        });

        modalsControl.modalsSubject.subscribe((data) => {
            if (data[this.name()]) {
                let state = Object.assign({}, this.state);
                state.modal = data[this.name()];
                this.setState(state);
            }
        });
    }

    getWeekNumber(d) {
        // Copy date so don't modify original
        d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
        // Set to nearest Thursday: current date + 4 - current day number
        // Make Sunday's day number 7
        d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay()||7));
        // Get first day of year
        var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
        // Calculate full weeks to nearest Thursday
        var weekNo = Math.ceil(( ( (d - yearStart) / 86400000) + 1)/7);
        // Return array of year and week number
        return [d.getUTCFullYear(), weekNo];
    }

    onButtonClick(e) {
        //get a list of all ticket during the month
        let dateEnd = new Date();
        let dateStart = new Date();
        dateStart.setDate(dateStart.getDate() - 40);

        let state = Object.assign({}, this.state);
        state.report = "Loading, wait about 1 minute...";
        this.setState(state);

        setTimeout(async () => {
            let report = '';

            let days = await tasks.loadDaysBetween(dateStart, dateEnd);
            for (let day of days) {
                report += '\n';
                report += 'DAY: ' + day.name + ' - week: ' + this.getWeekNumber(day.dateObj) + '\n';

                for (let task of day.tasks) {
                    report += '-- ' + task.name + ' -- ' + spentFormat(task.seconds) + '\n';
                }
                report += '\n';
            }

            let state = Object.assign({}, this.state);
            state.report = report;
            this.setState(state);
        }, 500);
    }

    render() {

        return (
            <div className={"modal report-modal " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>Report for the last month</h3>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        <button onClick={(e) => this.onButtonClick(e)} > Generate report </button>
                    </div>
                    <textarea className="report-view" value={this.state.report} readOnly={true} />
                </div>
            </div>
        );
    }
}