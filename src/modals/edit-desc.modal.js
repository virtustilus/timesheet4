import React from 'react';
import { ModalData } from './modal-data';
import { modalsControl } from './modals-control';

export class EditDescModal extends React.Component {

    name() {
        return 'EditDescModal';
    }
    onClose(e) {
        modalsControl.hideModal(this.name());
    }

    constructor(props) {
        super(props);
        this.state = {
            modal: new ModalData(false, null, null, '')
        };
        this.oldValue = '';

        modalsControl.modalsSubject.subscribe((data) => {

            if (data[this.name()]) {
                let state = Object.assign({}, this.state);
                state.modal = data[this.name()];
                this.oldValue = state.modal.value;
                this.setState(state);
            }
        });
    }

    updateValue(e) {
        let prevValueParts = this.state.modal.value.split('---');
        let state = Object.assign({}, this.state);
        if (prevValueParts.length > 1) {
            state.modal.value = prevValueParts[0] + '---' + e.target.value;
        } else {
            state.modal.value = e.target.value;
        }
        this.setState(state);
    }

    onButtonClick(e) {
        let callback = this.state.modal.callback;
        if (callback) {
            callback(this.state.modal.value);
        }
        modalsControl.hideModal(this.name());
    }

    removeTicket(e) {
        let state = Object.assign({}, this.state);
        let parts = state.modal.value.split('---');
        if (parts.length > 1) {
            state.modal.value = parts[1];
        }
        this.setState(state);
    }

    renderTicket() {
        let parts = this.state.modal.value.split('---');
        if (parts.length > 1) {
            return (
                <div className="ticket-name">
                        <span>{parts[0]}</span>
                        <span className="close-button" onClick={(e)=>this.removeTicket(e)}>&times;</span>
                </div>
            );
        }

        return '';
    }

    render() {

        
        let parts = this.state.modal.value.split('---');
        let name = parts[0];
        if (parts.length > 1) {
            name = parts[1];
        }

        return (
            <div className={"modal edit-desc-modal " + (this.state.modal.visible ? 'in' : '')}>
                <div className="modal-body">
                    <h3>Edit description of:</h3>
                    <h4>{this.oldValue}</h4>
                    <div className="close" onClick={(e)=>this.onClose(e)}>&times;</div>
                    <div>
                        {this.renderTicket()}
                        <span>Description:</span>
                        <textarea type="text" className="input" value={name}
                               onChange={(e) => this.updateValue(e)}
                               onKeyUp={(e) => this.updateValue(e)}></textarea>
                    </div>
                    <div>
                        <button onClick={(e)=>this.onButtonClick(e)}>Update</button>
                    </div>
                    <hr/>
                    <div>
                        To replace ticket name, click X and just add new one ticket name into description, like: <br/> CU-37kkas some description of activity.
                    </div>
                </div>
            </div>
        );
    }
}