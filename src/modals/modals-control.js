import { ASubject } from '../a.subject';
import { ModalData } from './modal-data';

class ModalsControl {
    constructor() {
        this.modals = {};
        this.modalsSubject = new ASubject();
    }

    showModal(name, modalData) {
        modalData = modalData || new ModalData(false, null);
        modalData.visible = true;
        this.modals[name] = modalData;
        this.modalsSubject.next(this.modals);
    }

    hideModal(name) {
        this.modals[name].visible = false;
        this.modalsSubject.next(this.modals);
    }
}

export const modalsControl = new ModalsControl();
