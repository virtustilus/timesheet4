import { stamp, slug } from './functions';
import { db } from './db.service';
import { ASubject } from './a.subject';

class Settings {

    async insertNew(name, type, value) {
        this.settings.data[name] = {
            name: name,
            type: type,
            value: value
        };
        return await db.run('INSERT INTO settings VALUES(?, ?, ?)', [name, type, value]);
    }
    
    async ensureSetting(name, type, value) {
        let row = await db.get('SELECT * FROM settings WHERE name = ?', [name]);
        if (!row) {
            await this.insertNew(name, type, value);
        } else if (row.type !== type) {
            await db.run('UPDATE settings SET type = ? WHERE name = ?', [type, name]);
        }
    }

    getValue(name) {
        let row = this.settings.data[name];
        if (row.type == 'integer') {
            return parseInt(row.value);
        }
        if (row.type == 'float') {
            return parseFloat(row.value);
        }
        return row.value;
    }

    getObject(name) {
        return this.settings.data[name];
    }

    async setValue(name, value) {
        await db.run('UPDATE settings SET value = ? WHERE name = ?', [value, name]);
        this.settings.data[name].value = value;
        this.settings.next(this.settings.data);
    }

    getNames() {
        return Object.keys(this.settings.data);
    }

    getAllSettings() {
        return Object.assign({}, this.settings.data);
    }

    constructor() {
        let sql = 'CREATE TABLE IF NOT EXISTS settings(' +
            'name TEXT' +
            ', type TEXT' +
            ', value TEXT' +
            ')';

        this.settings = new ASubject();
        this.settings.data = {};
        
        db.run(sql, []).then( async () => {
            //on application run we will recreate settings if needed, or fix the type
            
            // -- Shared API settings --
            await this.ensureSetting('API_URL', 'string', 'https://api.clickup.com/api/v2/');  // https://xxxxcompanyxxxx.atlassian.net/
            await this.ensureSetting('SKYPE_TICKET', 'string', '6yxvd');
            await this.ensureSetting('ADHOC_TICKET', 'string', '6yded');

            // -- JIRA Related settings --
            await this.ensureSetting('JIRA_LIST_OF_PROJECTS', 'text', 'AAA,BBB,CCC');
            await this.ensureSetting('JIRA_USERNAME', 'string', 'walter.white');
            await this.ensureSetting('JIRA_PASSWORD', 'password', 'xxxyyy');
            await this.ensureSetting('JIRA_SEARCH_TO_WORK', 'text', 'assignee = currentUser() ' +
                'AND project != XX ' +
                'AND (status in ("Selected For Development", "In Progress") ' +
                'OR status in ("Ready For Dev", "Dev In Progress", "Blocked") AND project in (AAA,BBB)) ' +
                'ORDER BY updated DESC');

            // -- ClickUp related settings --
            await this.ensureSetting('CLICK_UP_API_TOKEN', 'password', 'pk_3333333_918891je21XXXXXXXXXXX');
            await this.ensureSetting('CLICK_UP_TEAM_ID', 'string', '2530469');
            await this.ensureSetting('CLICK_UP_USER_ID', 'string', '3333333');
            
            // -- Other settings --
            await this.ensureSetting('CUSTOM_TASKS', 'text', 'Lunch,Private Time,Daily call,PM Chat,CEO chat,Check emails,Estimations,Planning');
            await this.ensureSetting('TIME_OFFSET', 'string', '09:00');
            await this.ensureSetting('COMPANY_TIME_OFFSET', 'string', '+2');

            //now load all settings, because we don't need to wait for db, every time we need some setting
            let settings = {};
            let rows = await db.all('SELECT * FROM settings', []);
            for (let row of rows) {
                settings[row.name] = row;
            }
            this.settings.next(settings);
        });
    }
}

export let settings = new Settings();

