export function stamp(date) {
    return parseInt(date.getTime() / 1000);
}

export function fromStamp(timestamp) {
    let date = new Date();
    date.setTime(timestamp * 1000);
    return date;
}

export function toDate(stamp) {
    let d = new Date();
    d.setTime(stamp + 1000);
    return d.toLocaleDateString();
}

export function toTime(stamp) {
    if (stamp == 0) {
        return '-';
    }
    let d = new Date();
    d.setTime(stamp * 1000);
    return d.toTimeString().replace(/^(\d+:\d+)\:.*/, '$1');
}

export function spentFormat(seconds){
    seconds = parseInt(seconds);
    let days = Math.floor(seconds / 86400);
    let hours = Math.floor(seconds % 86400 / 3600);
    let minutes = Math.floor(seconds % 86400 % 3600 / 60);
    let result = [];
    if (days > 0) {
        result.push(days + 'd');
    }
    if (hours > 0) {
        result.push(hours + 'h');
    }
    if (minutes > 0) {
        result.push(minutes + 'm');
    }
    if (seconds > 0 && seconds < 60) {
        result.push(seconds + 'sec');
    }
    if (result.length == 0) {
        result.push('0m');
    }
    return result.join(' ');
}

export function validateSpent(spent){
    return /^\s*(\d+h\s*)?(\d+m\s*)?(\d+s\s*)?(\d+)?\s*$/.test(spent);
}

export function spentToSeconds(spent){
    let seconds = 0;
    if (/^\s*(\d+h\s*)?(\d+m\s*)?(\d+s\s*)?(\d+)?\s*$/.test(spent)) {
        let m = spent.match(/^\s*((\d+)h\s*)?((\d+)m\s*)?((\d+)s\s*)?(\d+)?\s*$/);
        seconds += (m[2] ? m[2] : 0) * 3600 + (m[4] ? m[4] : 0) * 60 + (m[6] ? m[6] : 0) * 1 + (m[7] ? m[7] : 0) * 60;
    }
    return seconds;
}

export function slug(name) {
    return name.replace(/\s/g, '');
}

export function datetime(date) {
    let str = date.toString();
    //Sat Apr 07 2018 12:45:43 GMT+0300 (MSK)   ->   Apr 07 2018 12:45:43
    return str.replace(/^\w+\s+/, '').replace(/\s+\w+\+\d+\s+\(.*?\)$/, '')
}

export function datetimeDate(date) {
    let str = date.toString();
    //Sat Apr 07 2018 12:45:43 GMT+0300 (MSK)   ->   Apr 07 2018
    return str.replace(/^\w+\s+/, '').replace(/\s+\d+:\d+:\d+\s+\w+\+\d+\s+\(.*?\)$/, '')
}

export function dateTimeWeekDay(date) {
    const day = date.getDay();
    const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
    return days[day];
}

export function dateTimeDateAndWeek(date) {
    const day = date.getDay();
    const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
    let month = date.getMonth() + 1;
    if (month < 10) {
        month = '0' + month;
    }
    return date.getDate() + '-' + month + '-' + date.getFullYear() + ' ' + days[day];
}
