const http = require('http');
const https = require('https');
const querystring = require('querystring');
import { settings } from './settings.service';

import { spentFormat, fromStamp, stamp } from './functions';

function z(num) {
    if (num < 10) {
        num = '0' + num;
    }
    return num;
}

export class JiraAPI {

    constructor() {
        this.url = settings.getValue('API_URL');
        this.auth = `${settings.getValue('JIRA_USERNAME')}:${settings.getValue('JIRA_PASSWORD')}`;
        this.apiUrl = this.url + 'rest/api/latest';
        this.username = settings.getValue('JIRA_USERNAME');
        this.queryToWork = settings.getValue('JIRA_SEARCH_TO_WORK');
    }

    jiraDateTimeFormat(date) {
        //2018-04-07T20:47:03.000+0000
        return date.getFullYear() + '-' + z(date.getMonth() + 1) + '-' + z(date.getDate())
            + 'T' + z(date.getHours()) + ':' + z(date.getMinutes()) + ':' + z(date.getSeconds())
            + '.000+0100';
    }

    jiraDateFormat(date) {
        //2018-04-07
        return date.getFullYear() + '-' + z(date.getMonth() + 1) + '-' + z(date.getDate());
    }

    makeBasicOptions(url) {
        let m = url.match(/^(\w+)\:\/\/([^\/]+)(:(\d+))?(\/.*)$/);
        if (!m) {
            throw 'apiURL is incorrect';
        }
        let port = 80;
        if (m[1] == 'https') {
            port = 443;
        }
        return {
            hostname: m[2],
            port: (m[3] ? m[4] : port),
            path: m[5],
            agent: false,
            auth: this.auth
        };
    }

    doRequest(url, postData) {
        let options = this.makeBasicOptions(url);
        let body = null;
        if (postData) {
            body = JSON.stringify(postData);
            options.method = 'POST';
            options.headers = {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(body)
            };
        }
        let scheme = http;
        if (this.apiUrl.match(/^https/)) {
            scheme = https;
        }
        return new Promise((resolve, reject) => {
            let content = '';
            const req = scheme.request(options, (res) => {
                // console.log(`STATUS: ${res.statusCode}`);
                // console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
                res.setEncoding('utf8');
                res.on('data', (chunk) => {
                    content += chunk;
                });
                res.on('end', () => {
                    // console.log(`CONTENT: ${content}`);
                    resolve(content);
                });
            });

            req.on('error', (e) => {
                console.error(`problem with request: ${e.message}`);
                reject(e);
            });
            if (postData) {
                req.write(body);
            }
            req.end();
        });
    }

    static support(url) {
        return url.match(/\.atlassian\.net/);
    }

    generateLink(ticket) {
        if (!ticket) {
            return '';
        }
        return this.url + 'browse/' + ticket;
    }

    async getTicketSummary(ticket) {
        let content = await this.doRequest(this.apiUrl + '/issue/' + ticket + '?fields=summary,status,timetracking');
        //console.log('Got from server', content);
        let data = JSON.parse(content);
        if (data['fields']) {
            return {
                summary: data['fields']['summary'],
                status: data['fields']['status']['name'],
                timetracking: data['fields']['timetracking']
            }
        }
        return null;
    }

    async postWorkLog(ticket, startAt, seconds, description, remaining) {
        let url = this.apiUrl + '/issue/' + ticket + '/worklog';
        if (remaining) {
            //should be in format like 1h 12m
            url += '?adjustEstimate=new&newEstimate=' + remaining;
        }
        let date = new Date();
        date.setTime(startAt * 1000);

        let data = {
            started: this.jiraDateTimeFormat(date),
            comment: description,
            timeSpent: spentFormat(seconds)
        };

        return await this.doRequest(url, data);
    }

    async loadWorkLogsBetweenDates(startAt, stopAt) {
        let dateFrom = this.jiraDateFormat(fromStamp(startAt));
        let dateTo = this.jiraDateFormat(fromStamp(stopAt));

        let url = this.apiUrl + '/search';
        let query = `worklogDate >= '${dateFrom}' AND worklogDate <= '${dateTo}'`
            + ` AND worklogAuthor = currentUser() ORDER BY updated DESC`;
        let params = { jql: query, startAt: 0, maxResults: 100 };
        let content = await this.doRequest(url, params);
        let data = JSON.parse(content);

        let prevLogs = [];

        for (let issue of data['issues']) {
            let key = issue['key'];
            let worklogData = JSON.parse(await this.doRequest(this.apiUrl + `/issue/${key}/worklog`));

            for (let worklog of worklogData['worklogs']) {
                if (worklog['author']['key'] !== this.username && worklog['author']['emailAddress'] !== this.username) {
                    continue;
                }

                let s = worklog['started'].replace(/^(\d+\-\d+\-\d+)T.*$/, "$1");
                if (stamp(new Date(s)) < startAt) {
                    continue;
                }
                if (stamp(new Date(s)) > stopAt) {
                    continue;
                }
                let timeSpent = worklog['timeSpentSeconds'];
                let eta = issue['fields']['timeestimate'];
                //console.log(issue);
                if (eta === null || eta === 0) {

                    if (issue['customfield_10004']) {
                        eta = issue['customfield_10004'] * 2;
                    } else {
                        eta = 0;
                    }
                }
                let spent = issue['fields']['timespent'];
                if (spent === null) {
                    spent = 0;
                }
                if (spent === 0 && eta === 0) {
                    spent = 0.1;
                }

                let prevLog = {};
                prevLog['issue'] = key;
                prevLog['summary'] = issue['fields']['summary'];
                prevLog['link'] = this.generateLink(key);
                prevLog['worked'] = Math.round(10.0 * timeSpent / 3600) / 10;
                prevLog['remaining'] = Math.round(10.0 * eta / 3600) / 10;
                prevLog['percent'] = Math.floor(Math.round(100 - 100.0 * eta / (eta + spent)));
                prevLog['comment'] = worklog['comment'];
                prevLog['status'] = issue['fields']['status']['name'];

                prevLogs.push(prevLog);
            }
        }
        return prevLogs;
    }

    async loadToWorkTickets() {

        let url = this.apiUrl + '/search';
        let query = this.queryToWork;
        let params = { jql: query, startAt: 0, maxResults: 100 };
        let content = await this.doRequest(url, params);
        let data = JSON.parse(content);

        let toWork = [];

        for (let issue of data['issues']) {
            let key = issue['key'];
            let eta = issue['fields']['timeestimate'];
            let spent = issue['fields']['timespent'];
            if (eta === null) {
                eta = 0;
            }
            if (spent === null){
                spent = 0;
            }
            if (spent == 0 && eta == 0) {
                spent = 0.1
            }

            let item = {};
            item['issue'] = key;
            item['summary'] = issue['fields']['summary'];
            item['link'] = this.generateLink(key);
            item['remaining'] = Math.round(10.0 * eta / 3600) / 10;
            item['percent'] = Math.floor(Math.round(100 - 100.0 * eta / (eta + spent)));

            toWork.push(item);
        }

        return toWork;
    }

    getTicketEta(ticket) {

    }

    getTicketSpellVersions(ticket) {
        return [ticket, this.apiUrl  + '/browse/' + ticket];
    }

    ticketByName(name) {
        let projects = settings.getValue('JIRA_LIST_OF_PROJECTS').split(',');
        let pattern = '(^|\\W)(' + projects.map((p) => p + '\\-\\d+').join('|') + ')';
        let m = name.match(new RegExp(pattern));
        if (m) {
            return m[2];
        }
        return '';
    }
}
