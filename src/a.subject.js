import { Observable, Subject } from 'rxjs/Rx';

export class ASubject {

    constructor() {
        this.data = undefined;
        this.subject = new Subject();
    }

    unsubscribe() {
        return this.subject.unsubscribe();
    }

    next(value) {
        this.data = value;
        return this.subject.next(value);
    }

    value() {
        return this.data;
    }

    subscribe(subscriber) {
        //let v = this.subject._subscribe(subscriber);
        let v = this.subject.asObservable().subscribe(subscriber);
        if (this.data !== undefined) {
            subscriber(this.data);
        }
        return v;
    }

    asObservable() {
        return this.subject.asObservable();
    }
}

