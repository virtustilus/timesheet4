
# Install dependencies
 
    sudo npm install -g electron 
    npm install
    npm run postinstall

# Prod usage

Now you can start it by running:

    npm run build
    npm start


# Dev usage
You will need 2 terminals, one with:

    npm run watch
    
And second one with:

    npm run start-dev


# Build project for prod use
    

    npm run dist


#### To create linux package: 

    npm run dish-linux

:)

### Jira login
As server url you will need to set something like: 
https://XXX.atlassian.net/
But for your company server.

For authentication you have to use a token.

To create a new token go here:
https://id.atlassian.com/manage/api-tokens
and create one.
Use token in the setting JIRA_PASSWORD in this tracker app.
Also fill email into JIRA_USERNAME.



### ClickUp integration
Use this URL for API_URL parameter: 
https://api.clickup.com/api/v2/


You will need to go into clickup Settings -> Apps.
In the URL you will see a Team ID, for example: https://app.clickup.com/8880999/settings/apps, team id is 8880999.
Put this team ID into CLICK_UP_TEAM_ID in tracker settings.

Then generate new API Token on this ClickUp page.
It will look like: pk_3333333_918891je21XXXXXXXXXXX, where 3333333 - is your User Id.
Now put it into CLICK_UP_API_TOKEN in tracker settings.
Fill up CLICK_UP_USER_ID also with User id from token.

Check it now by starting new task with ticket URL in the name - it should be automatically replaced by ticket name.

You can also fill up SKYPE_TICKET and ADHOC_TICKET with ticket ids (for example: 5ycccc )


### Notes
Time slots with name "Lunch" and "Private Time" won't be logged into JIRA or ClickUp and won't be counted as working time.
